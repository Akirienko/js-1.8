let input = document.getElementById("input");
let span = document.createElement("span");
let spanDel = document.createElement("span");
let spanErr = document.createElement("span");
let label = document.getElementById("label");

input.addEventListener("focus", function(){
  input.classList.add("valid");
  if (input.classList.contains("invalid")) {
    input.classList.remove("invalid");
    spanErr.remove();
  }
});

input.addEventListener("blur", function(){
  if (input.value < 0) {
    spanErr.textContent = `Please enter correct price`; 
    spanErr.style.cssText = "color : red; display : block;";
    input.classList.add("invalid");
    if (input.classList.contains("valid")) {
      input.classList.remove("valid");
    }
    label.after(spanErr);
  } else {    
    input.classList.add("valid");
    spanDel.textContent = `x`;
    spanDel.style.cssText = "cursor : pointer; color : red; margin-left : 30px;"
    span.style.color = "green";
    span.textContent = `Текущая цена: ${input.value}`;
    document.body.prepend(span);
    span.append(spanDel);

    spanDel.addEventListener("click", function(){
      span.remove();
      input.value = "";
    })
  }
  
});

